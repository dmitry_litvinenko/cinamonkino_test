**Description:**
This test is created for cinamonkino.com on the Cucumber and Selenium framework

**Required plugins for Intellij Idea:**
Cucumber for Java and Gherkin

**How to launch test:**
1. Insert path to chrome driver constant  CHROME_DRIVER_LOCATION = "path"; In the BaseFunction.class;
Chrome driver will be present in following folder: \cinamon_kino_test\chrome_driver\2.41 (For linux and windows);
2. Install required plugins;
3. Go to RunTests.class ;
4. Press run;

**Notes:**
Tests have been tested with Windows OS, Chrome Driver: 2.41 and Chrome browser v68;