package core;

import com.google.common.base.CharMatcher;
import org.openqa.selenium.By;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class OrderHelper {

    private static final By POP_UP = By.cssSelector("[class='close-reveal-modal']");

    public Integer getRandomNumber(Integer bound) {
        Random rand = new Random();
        return rand.nextInt(bound) + 1;
    }

    public Integer stringToInt(String textInput) {
        String digits = CharMatcher.digit().retainFrom(textInput);
        return Integer.valueOf(digits);
    }

    public void closePopUp(BaseFunctions bs) {
        if (bs.isElementNotEmpty(POP_UP)) {
            bs.clickJsElement(POP_UP).sleep(1);
        }
    }

    public String getRandomDate(int rangeOfRandomDays) {
        Date startDate = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        int randomNum = ThreadLocalRandom.current().nextInt(1, rangeOfRandomDays + 1);
        Date randomDay = addDays(startDate, randomNum);
        return dateFormat.format(randomDay);
    }

    private Date addDays(Date date, int days) {
        date.setTime(date.getTime() + days * 1000 * 60 * 60 * 24);
        return date;
    }
}