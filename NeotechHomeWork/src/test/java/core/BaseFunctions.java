package core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class BaseFunctions {

    private WebDriver driver;
    private static final String CHROME_DRIVER_LOCATION = "D://Workspace/cinamon_kino_test/chrome_driver/2.41/windows/chromedriver.exe";
    private static final Logger LOGGER = LogManager.getLogger(BaseFunctions.class);

    public BaseFunctions() {
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_LOCATION);

        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "Nexus 7");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        LOGGER.info("Starting Chrome Driver");
        this.driver = new ChromeDriver(chromeOptions);
    }

    /**
     * This method goes to URL
     *
     * @param url web address
     */
    public void openWebPage(String url) {
        if (!url.contains("http://") && !url.contains("https://")) {
            url = "http://" + url;
        }
        driver.get(url);
    }

    /**
     * Method click on displayed web element
     *
     * @param locator element to click
     */
    public void clickElement(By locator) {
        driver.findElement(locator).click();
    }

    /**
     * Method click on the web element
     * trough Java Script
     *
     * @param locator element to click
     */
    public BaseFunctions clickJsElement(By locator) {
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
        return this;
    }

    /**
     * Method click on the web element
     * trough Java Script
     *
     * @param locator element to click
     */
    public BaseFunctions clickJsElement(String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
        return this;
    }

    /**
     * Method click on the specific web element
     * trough Java Script
     *
     * @param locator element to click
     * @param index   element number
     */
    public void clickJsElements(By locator, int index) {
        WebElement element = driver.findElements(locator).get(index);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    /**
     * Method gives boolean output if web element present
     *
     * @param locator element to click
     */
    public boolean isElementNotEmpty(By locator) {
        return !driver.findElements(locator).isEmpty();
    }

    /**
     * Method provide thread sleep in seconds
     *
     * @param seconds - time to sleep
     */
    public BaseFunctions sleep(Integer seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Method send keys to input field
     *
     * @param locator element to send key
     */
    public void sendKeys(By locator, String key) {
        driver.findElement(locator).sendKeys(key);
    }

    /**
     * Method simulate keyboard keys
     *
     * @param locator element to simulate keyboard keys
     * @param key     keyboard keys
     */
    public void pressEnter(By locator, Keys key) {
        driver.findElement(locator).sendKeys(key);
    }

    /**
     * Method getting attribute from element
     *
     * @param locator element to get attribute
     * @param value   attribute name
     */
    public String getElementAttribute(By locator, String value) {
        return driver.findElement(locator).getAttribute(value);
    }

    /**
     * Method get text from element
     *
     * @param locator element to get text
     */
    public String getTextFromElement(By locator) {
        return driver.findElement(locator).getText();
    }

    /**
     * Method get list of web elements
     *
     * @param locator element to get list
     */
    public List<WebElement> getElementsList(By locator) {
        return driver.findElements(locator);
    }

    /**
     * Method get wait while web element will not appear
     *
     * @param locator element to wait
     * @param seconds time to wait
     */
    public WebElement waitFor(By locator, Integer seconds) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(seconds, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        return wait.until(driver1 -> driver1.findElement(locator));
    }

    /**
     * Method return number between minimal and maximal range
     */
    public Integer getRandomNumberInRange(Integer min, Integer max) {
        Random r = new Random();
        return r.nextInt((max + 1) - min) + min;
    }

    /**
     * Method to close the web driver
     */
    public void closeDriver() {
        LOGGER.info("Closing chrome driver");
        driver.close();
    }

}