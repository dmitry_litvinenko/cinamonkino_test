package pageObjects;

import core.BaseFunctions;
import core.OrderHelper;
import org.openqa.selenium.By;

public class CheckOutPage {

    private BaseFunctions baseFunc;
    private OrderHelper orderHelper = new OrderHelper();
    private static final By TOTAL_SUM = By.cssSelector("[class='total']");

    public CheckOutPage(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public Integer getTotalSum(){
        return orderHelper.stringToInt(baseFunc.getTextFromElement(TOTAL_SUM));
    }

}
