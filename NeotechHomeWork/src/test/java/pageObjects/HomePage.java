package pageObjects;

import core.BaseFunctions;
import core.OrderHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomePage {

    private BaseFunctions baseFunc;
    private static final Logger LOGGER = LogManager.getLogger(HomePage.class);
    private static final By USER_NICK_NAME = By.cssSelector("[class='username']");
    private static final By LOG_OUT_BUTTON = By.cssSelector("[class='logout']");
    private static final By MOVIES_URL = By.xpath("//a[@class='time-wrap']");
    private static final String HOME_PAGE_URL = "alfa.cinamonkino.com";
    private OrderHelper oh = new OrderHelper();

    public HomePage(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public void waitForLogIn() {
        baseFunc.waitFor(LOG_OUT_BUTTON, 10);
        LOGGER.info("User logged in\n");
    }

    public String getUserNickName() {
        return baseFunc.getTextFromElement(USER_NICK_NAME);
    }

    public String openRandomMovie() {
        LOGGER.info("Open random movie");
        List<WebElement> list = baseFunc.getElementsList(MOVIES_URL);
        return list.get(oh.getRandomNumber(list.toArray().length)).getAttribute("href");
    }

    public void logOut(){
        baseFunc.openWebPage(HOME_PAGE_URL);
        LOGGER.info("LOG OUT");
        baseFunc.clickJsElement(LOG_OUT_BUTTON);
    }
}
