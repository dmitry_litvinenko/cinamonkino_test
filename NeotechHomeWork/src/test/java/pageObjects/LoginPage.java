package pageObjects;

import core.BaseFunctions;
import core.OrderHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

public class LoginPage {

    private BaseFunctions baseFunc;
    private OrderHelper orderHelper = new OrderHelper();
    private static final Logger LOGGER = LogManager.getLogger(LoginPage.class);
    private static final String HOME_PAGE_URL = "alfa.cinamonkino.com";
    private static final String SIGN_IN = "/en/my-account/sign-in/";
    private static final By LOGIN_KEY = By.cssSelector("[name='username']");
    private static final By PASSWORD_KEY = By.cssSelector("[name='password']");
    private static final By SUBMIT_BUTTON = By.cssSelector("[type='submit']");

    public LoginPage(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public LoginPage openSignIn() {
        baseFunc.openWebPage(HOME_PAGE_URL + SIGN_IN);
        orderHelper.closePopUp(baseFunc);
        return this;
    }

    public LoginPage sendKeys(String login, String password) {
        LOGGER.info("Sending login credentials");
        baseFunc.sendKeys(LOGIN_KEY, login);
        LOGGER.info("Sending password credentials");
        baseFunc.sendKeys(PASSWORD_KEY, password);
        return this;
    }

    public LoginPage isLoginFieldsDisplayed() {
        LOGGER.info("Checking is login field displayed");
        baseFunc.isElementNotEmpty(LOGIN_KEY);
        LOGGER.info("Checking is password field displayed");
        baseFunc.isElementNotEmpty(PASSWORD_KEY);
        return this;
    }

    public LoginPage pressSubmitButton() {
        LOGGER.info("Log in to the user profile");
        baseFunc.clickJsElements(SUBMIT_BUTTON, 0);
        return this;
    }
}