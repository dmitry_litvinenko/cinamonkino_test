package pageObjects;

import core.BaseFunctions;
import core.OrderHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

public class SeatsOrderPage {

    private BaseFunctions baseFunc;
    private OrderHelper orderHelper = new OrderHelper();
    private static final Logger LOGGER = LogManager.getLogger(SeatsOrderPage.class);
    private static final By ROW_NUMBERS = By.cssSelector("class='row-nr'");
    private static final By SEAT_NUMBERS = By.cssSelector("class='seat-nrs'");
    private static final By SELECDET_SEAT = By.cssSelector("class='reserved normal'");
    private static final By RESERVED_SEAT = By.cssSelector("class='broken normal'");
    private static final By PAYMENT_PAGE = By.xpath("//input[@type='submit']");
    private static final String FREE_SEAT = "//*[@id='a1-r%d-s%d' and @class='empty normal']";

    public SeatsOrderPage(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public void goToPaymentPage() {
        LOGGER.info("Moving to payment page");
        baseFunc.clickJsElement(PAYMENT_PAGE);
        orderHelper.closePopUp(baseFunc);
    }

    public void getRandomSeatInRowRange(int firstRow, int lastRow) {
        baseFunc.clickJsElement(String.format(FREE_SEAT, getRandomRowInRange(firstRow, lastRow), getRandomSeatNumber()));
    }

    private Integer getRandomRowInRange(int firstRow, int lastRow) {
        return convertRow(baseFunc.getRandomNumberInRange(firstRow, lastRow)) + getLastRow();
    }

    private Integer getRandomSeatNumber() {
        return baseFunc.getRandomNumberInRange(0, getLastSeat());
    }

    private Integer getLastRow() {
        Integer count = 0;
        String lastRow = "a1-r0-s3";
        while (baseFunc.isElementNotEmpty(By.id(lastRow))) {
            lastRow = String.format("a1-r%d-s3", count++);
        }
        return orderHelper.stringToInt(lastRow.substring(4, 6)) - 1;
    }

    private Integer getLastSeat() {
        Integer count = 3;
        String lastSeat = "a1-r3-s3";
        while (baseFunc.isElementNotEmpty(By.id(lastSeat))) {
            lastSeat = String.format("a1-r3-s%d", count++);
        }
        return Integer.parseInt(lastSeat.substring(7));
    }

    private int convertRow(int rowNumber) {
        return (rowNumber - (rowNumber * 2) + 1);
    }

}
