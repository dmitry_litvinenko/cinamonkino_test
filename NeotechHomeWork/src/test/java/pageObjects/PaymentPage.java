package pageObjects;

import core.BaseFunctions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;


public class PaymentPage {

    private BaseFunctions baseFunc;
    private static final Logger LOGGER = LogManager.getLogger(PaymentPage.class);
    private static final By PM_SEB = By.xpath("//li[@class='seblv']");
    private static final By PM_CITADELE = By.xpath("//li[@class='parexlv']");
    private static final By PM_SWEDBANK = By.xpath("//li[@class='hanzalv']");
    private static final By PM_MASTERVISA = By.xpath("//li[@class='lv_lpb']");

    public PaymentPage(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    private List<By> getPaymentsMethods() {
        List<By> list = new ArrayList<>();
        list.add(PM_SEB);
        list.add(PM_CITADELE);
        list.add(PM_SWEDBANK);
        list.add(PM_MASTERVISA);
        return list;
    }

    public void assertPaymentsAvailability() {
        LOGGER.info("Asserting payment availability");
        for (By element : getPaymentsMethods()) {
            baseFunc.sleep(1);
            assertTrue((baseFunc.isElementNotEmpty(element)));
        }
    }

}
