package pageObjects;

import core.BaseFunctions;
import core.OrderHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertTrue;

public class TicketOrderPage {

    private BaseFunctions baseFunc;
    private OrderHelper orderHelper = new OrderHelper();
    private static final Logger LOGGER = LogManager.getLogger(TicketOrderPage.class);
    private static final By TICKET_PRICE = By.xpath("//td[@class='single-price']");
    private static final By SEAT_ORDER_PAGE = By.id("next-step");
    private static final By ADD_TICKET = By.xpath("//input[@field='ticket[0003][quantity]']");
    private static final By TOTAL_PRICE = By.cssSelector("[class='total']");
    private static final By COUPON_ERROR = By.xpath("//tr[@class='voucher error']");
    private static final By COUPON_FIELD = By.cssSelector("[placeholder='Enter voucher or discount code']");
    private static final String TICKET_ORDER_PAGE = "https://alfa.cinamonkino.com/en/choose-tickets/";

    public TicketOrderPage(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public int getStudentTicketPrice() {
        return orderHelper.stringToInt(baseFunc.getTextFromElement(TICKET_PRICE));
    }

    public Integer getTotalSum() {
        return orderHelper.stringToInt(baseFunc.getTextFromElement(TOTAL_PRICE));
    }

    public void addTickets(int ticketAmount) {
        LOGGER.info("Adding: " + " ticket/s");
        orderHelper.closePopUp(baseFunc);
        for (int i = 0; i < ticketAmount; i++) {
            baseFunc.clickJsElement(ADD_TICKET);
        }
    }

    public TicketOrderPage sendCouponKey(String couponKey) {
        baseFunc.sendKeys(COUPON_FIELD, couponKey);
        return this;
    }

    public TicketOrderPage submitCoupon() {
        LOGGER.info("Submitting coupon");
        baseFunc.pressEnter(COUPON_FIELD, Keys.ENTER);
        return this;
    }

    public void assertCouponValidation() {
        baseFunc.sleep(1);
        assertTrue(baseFunc.isElementNotEmpty(COUPON_ERROR));
    }

    public void goToSearOrderPage() {
        LOGGER.info("Moving to seat order page");
        baseFunc.clickJsElement(SEAT_ORDER_PAGE);
        orderHelper.closePopUp(baseFunc);

    }

    public void goToTicketPage() {
        baseFunc.openWebPage(TICKET_ORDER_PAGE);
    }

}
