package pageObjects;

import core.BaseFunctions;
import core.OrderHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

public class UserProfilePage {

    private BaseFunctions baseFunc;
    private OrderHelper orderHelper = new OrderHelper();
    private static final Logger LOGGER = LogManager.getLogger(UserProfilePage.class);
    private static final String USER_PROFILE_PAGE = "https://alfa.cinamonkino.com/en/my-account/";
    private static final By FIRST_NAME = By.cssSelector("[name='firstname']");
    private static final By SECOND_NAME = By.cssSelector("[name='lastname']");


    public UserProfilePage(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public void openProfilePage() {
        LOGGER.info("Opening user profile page: " + USER_PROFILE_PAGE);
        baseFunc.openWebPage(USER_PROFILE_PAGE);
        orderHelper.closePopUp(baseFunc);
        baseFunc.waitFor(FIRST_NAME, 10);
    }

    public String getFistName() {
        return baseFunc.getElementAttribute(FIRST_NAME, "value");
    }

    public String getSecondName() {
        return baseFunc.getElementAttribute(SECOND_NAME, "value");
    }
}
