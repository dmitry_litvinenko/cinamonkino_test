package stepsDefinitions;

import core.BaseFunctions;
import core.OrderHelper;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pageObjects.*;

import static org.junit.Assert.assertEquals;

public class TicketOrderStepDefs {

    // TODO ADD DI https://google.github.io/dagger/  or https://github.com/google/guice
    private static final Logger LOGGER = LogManager.getLogger(TicketOrderStepDefs.class);
    private BaseFunctions baseFunc = new BaseFunctions();
    private HomePage homePage = new HomePage(baseFunc);
    private LoginPage loginPage = new LoginPage(baseFunc);
    private UserProfilePage profilePage = new UserProfilePage(baseFunc);
    private TicketOrderPage ticketPage = new TicketOrderPage(baseFunc);
    private SeatsOrderPage seatsPage = new SeatsOrderPage(baseFunc);
    private OrderHelper orderHelper = new OrderHelper();
    private PaymentPage payments = new PaymentPage(baseFunc);
    private CheckOutPage checkOut = new CheckOutPage(baseFunc);

    private Integer totalTicketSum;

    @Before
    public void userLogin() {
        loginPage.openSignIn()
                .isLoginFieldsDisplayed()
                .sendKeys("dima110992@rambler.ru", "neotechtest")
                .pressSubmitButton();
        homePage.waitForLogIn();
    }

    @Given("Logged user with nickname (.*)")
    public void getUserNickName(String userNickName) {
        assertEquals(homePage.getUserNickName(), userNickName);
    }

    @When("User open profile")
    public void openUserProfile() {
        LOGGER.info("User open profile page");
        profilePage.openProfilePage();
    }

    @Then("Name and Surname should be: (.*) / (.*)")
    public void getNameAndSurname(String firstName, String secondName) {
        LOGGER.info("Asserting profile name and surname");
        assertEquals(firstName, profilePage.getFistName());
        assertEquals(secondName, profilePage.getSecondName());
    }

    @Then("User press coupon button without key")
    public void testCouponButton() {
        orderHelper.closePopUp(baseFunc);
        ticketPage.submitCoupon().assertCouponValidation();
    }

    @Then("User send false coupon keys: (.*)")
    public void testCouponValidation(String key) {
        LOGGER.info("Testing coupon validation");
        ticketPage.sendCouponKey(key).submitCoupon().assertCouponValidation();
    }

    @Given("Payments methods for random movie")
    public void navigateToPaymentMethods() {
        baseFunc.openWebPage(homePage.openRandomMovie());
        ticketPage.addTickets(1);
        ticketPage.goToSearOrderPage();
        seatsPage.goToPaymentPage();
    }

    @Then("User checking availability of payments opened:")
    public void testPaymentsAvailability() {
        payments.assertPaymentsAvailability();
    }

    @Given("Random movie, date and time")
    public void getMovieWithRandomDateAndTime() {
        baseFunc.openWebPage(homePage.openRandomMovie());
    }

    @When("User adding (.*) tickets")
    public void addTickets(int ticketAmount) {
        ticketPage.addTickets(ticketAmount);
        totalTicketSum = ticketPage.getStudentTicketPrice() * ticketAmount;
        assertEquals(totalTicketSum, ticketPage.getTotalSum());
    }

    @Then("Checking ticket sum in confirmation step")
    public void checkTicketsSum() {
        ticketPage.goToSearOrderPage();
        seatsPage.goToPaymentPage();
        assertEquals(totalTicketSum, checkOut.getTotalSum());
    }

    @When("User change ticket order")
    public void navigateToOrderPage() {
        ticketPage.goToTicketPage();
    }

    @And("User take random seats from row: (.*) or (.*)")
    public void getRandomPlace(int firstRow, int lastRow) {
        ticketPage.goToSearOrderPage();
        seatsPage.getRandomSeatInRowRange(firstRow, lastRow);
    }

    @After
    public void closeChromeDriver() {
        homePage.logOut();
        baseFunc.closeDriver();
    }

}

