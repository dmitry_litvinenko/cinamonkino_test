Feature: Ticket order - smoke functional test

  Scenario: Test user profile Name and Surname
    Given Logged user with nickname NEO TECH
    When User open profile
    Then Name and Surname should be: Neo / Tech

  Scenario: Test coupon validation
    Given Logged user with nickname NEO TECH
    And Random movie, date and time
    Then User press coupon button without key
    And User send false coupon keys: 12Coupon_3./

  Scenario: Test payments methods
    Given Payments methods for random movie
    Then User checking availability of payments opened:

  Scenario: Test ticket order
    Given Random movie, date and time
    When User adding 2 tickets
    Then Checking ticket sum in confirmation step
    When User change ticket order
    And User adding 1 tickets
    Then Checking ticket sum in confirmation step

  #Scenario: Smoke test on seat order validation
    #Given Random movie, date and time
    #When User adding 2 tickets
    #And User take random seats from row: 5 or 6
    #Then In confirmation step checking tickets sum and seats NOT DONE
    #When User change order to 1 student ticket NOT DONE
    #And Leaving randomly selected seats NOT DONE
    #Then In confirmation step checking tickets sum NOT DONE

